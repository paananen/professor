var fs = require("fs");
var TelegramBot = require("./TelegramBot");
var OpenWeatherMap = require("./OpenWeatherMap");
var bot = null;
var weatherAPI = null;
var weather = null;

fs.readFile("token.txt", "utf8", function (error, data) {
	if(error) {
		console.log(error);
	}
	bot = new TelegramBot(data);
	setInterval(getUpdates, 2000);
});

fs.readFile("open-weather-map-key.txt", "utf8", function (error, data) {
	if(error) {
		console.log(error);
	}
	weatherAPI = new OpenWeatherMap(data);
	updateWeather();
	setInterval(updateWeather, 600000);
});

function handleUpdates(updates) {
	if(updates.length == 0) {
		return false;
	}
	updates.forEach(handleUpdate);
}

function handleUpdate(update) {
	var message = update.message;
	if(typeof message.text == "string") {
		console.log(message.from.first_name + ": " + message.text);
		if(message.text.toLowerCase() == "hello") {
			sayHello(message.from.first_name, message.chat.id);
		} else if(message.text.toLowerCase().indexOf("kalja") > -1) {
			checkBeerFeasibility(message.chat.id);
		} else if(Math.random() > 0.95) {
			satanSuffix(message.text, message.chat.id);
		}
	}
}

function satanSuffix(text, chatID) {
	var response = "";
	switch(text.charAt(text.length - 1)) {
		case ".":
			response = text.slice(0, -1) + " saatana.";
			break;
		case "!":
			response = text.slice(0, -1) + " saatana!";
			break;
		case "?":
			response = text.slice(0, -1) + " saatana.";
			break;
		default:
			response = text + " saatana";
	}
	bot.sendMessage(response, chatID);
}

function sayHello(name, chatID) {
	bot.sendMessage("Hello, " + name, chatID);
}

function getUpdates() {
	bot.getUpdates(handleUpdates);
}

function updateWeather() {
	weatherAPI.getWeather(handleWeather);
}

function handleWeather(response) {
	weather = {
		temperature : response.main.temp - 272.15,
		code : response.weather[0].id
	};
	console.log(weather.temperature);
}

function checkBeerFeasibility(chatID) {
	if(!weather) {
		return false;
	}

	var shouldDrinkInside = true;
	var weatherType = "";

	switch(weather.code) {
		case 200:
		case 201:
		case 202:
		case 210:
		case 211:
		case 212:
		case 221:
		case 230:
		case 231:
		case 232:
			weatherType = "ukkosmyrsky";
			break;
		case 300:
		case 301:
		case 302:
		case 310:
		case 311:
		case 312:
		case 313:
		case 314:
		case 321:
		case 500:
		case 501:
		case 502:
		case 503:
		case 504:
		case 511:
		case 520:
		case 521:
		case 522:
		case 531:
			weatherType = "sataa vettä";
			break;
		case 600:
		case 601:
		case 602:
		case 620:
		case 621:
		case 622:
			weatherType = "sataa lunta";
			break;
		case 611:
		case 612:
		case 615:
		case 616:
			weatherType = "sataa räntää tai jotain töhnää";
			break;
		case 701:
		case 711:
		case 721:
		case 731:
		case 741:
		case 751:
		case 761:
		case 762:
		case 771:
		case 781:
			weatherType = "ilmassa on jotain";
			break;
		case 800:
		case 801:
		case 802:
		case 803:
			weatherType = "poutaa";
			shouldDrinkInside = false;
			break;
		case 804:
			weatherType = "pilvistä";
			shouldDrinkInside = false;
			break;
		case 900:
		case 901:
		case 902:
		case 903:
		case 904:
		case 905:
		case 906:
			weatherType = "sään jumalat haluaa tappaa jonkun";
			break;
		case 951:
		case 952:
		case 953:
			weatherType = "tyyntä";
			shouldDrinkInside = false;
			break;
		case 954:
		case 955:
		case 956:
		case 957:
		case 958:
		case 959:
		case 960:
		case 961:
		case 962:
			weatherType = "tuulista";
			break;
	}

	if(weather.temperature < 15) {
		shouldDrinkInside = true;
	}

	var response = "Siellä on " + Math.round(weather.temperature) + " astetta " + (weather.temperature >= 0 ? "lämmintä" : "pakkasta") + " ja " + weatherType + ", eli " + (shouldDrinkInside ? "kannattaa mennä sisälle juomaan" : "pussikaljakelit") + ".";

	bot.sendMessage(response, chatID);
}