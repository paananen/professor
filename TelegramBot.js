var https = require("https");
var urlHelper = require("./url-helper");

function TelegramBot(token) {
	this.token = token;
	this.lastUpdateID = 0;
	this.httpsOptions = {
		hostname: "api.telegram.org",
		port: "443",
		method: "GET"
	};
}

TelegramBot.prototype.sendRequest = function (command, parameters, responseHandler) {
	this.httpsOptions.path = this.generatePath(this.token, command, parameters);
	var request = https.request(this.httpsOptions, responseHandler);
	request.end();
}

TelegramBot.prototype.generatePath = function (token, command, parameters) {
	return "/" + token + "/" + command + (parameters === null ? "" : "?") + urlHelper.serializeParameters(parameters);
}

TelegramBot.prototype.getUpdates = function (updatesHandler) {
	var that = this;
	var parameters = {
		offset : this.lastUpdateID + 1
	};
	this.sendRequest("getUpdates", parameters, function (response) {
		response.on("data", function (data) {
			var result = JSON.parse(data).result;
			if(result.length > 0) {
				that.lastUpdateID = result[result.length - 1].update_id;
			}
			updatesHandler(result);
		});
	});
}

TelegramBot.prototype.sendMessage = function (text, chatID) {
	var parameters = {
		chat_id: chatID,
		text: text
	};
	this.sendRequest("sendMessage", parameters, function (response) {
		response.on("data", function (data) {
			console.log(JSON.parse(data));
		});
	});
}

module.exports = TelegramBot;