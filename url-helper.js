module.exports = {
	serializeParameters : function (parameters) {
		if(!parameters || typeof parameters !== "object") {
			return "";
		}
		return Object.keys(parameters).map(function (key) {
			return encodeURIComponent(key) + "=" + encodeURIComponent(parameters[key]);
		}).join("&");
	}	
};